FC Københavns kampe i Europa
============================

Show an interactive map of all away matches in Europa for FC København.

Can be seen in action here: http://fodbold.ougar.dk/fckobenhavn/europakort/

Previously used data from a google spreadsheet and imported data via
google Fusion. But Google discontinued Google Fusion, so now the data
is in a mariadb database, and fetched via an Ajax call in the javascript
file.

It is a very simple homepage:
* One php-file "index.php" which generates a very short html file
* One php-file "getdata.php" which is called with an Ajax request to get the data. This php-file includes
  a DB-library i use, but it symply fetches all the rows of a single MySQL table.
* One javascript file "fckeuropa.js" which has the google maps stuff and the filtering and grouping code
* One stylesheet file "fckeuropa.css"
* A few small images (emojii and menu icon and twitter card image)
