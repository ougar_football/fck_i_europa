var map, infoWindow;

var filter = {tournament: [], part: [], season: [], video: []};
var Groups={};
var Data;
var CurrentGroup="nogroup";
var debug;

$(document).ready(function() {          // {{{ Setup
  // Add click event to menu-icon to hide and show filter-menu
  // The menu button itself is also moved in and out and the 
  // image is changed between a dark and a light version.
  $('body').on('click','#menuicon',function(){
    if ($('#control').offset().left == 0) {
      $('#control').animate({left: -$('#control').width()-10});
      $('#toggle').animate({left: 6});
      $('#menuicon').attr("src","img/menu_dark.png");
      $('#legend').animate({left: 26});
      closeCredits();
    }
    else {
      $('#control').animate({left:0})
      $('#toggle').animate({left:142})
      $('#menuicon').attr("src","img/menu_light.png");
      $('#legend').animate({left: 170});
    }
  });
  if (isMobile()) hideMobileAddressBar();

  // Show info-window when "Credits and feedback" is clicked
  $('#contact').click(function() {
    $('#feedback').show();
  });

  if (controlTooLarge()) {
    $("#control").width("180px");
  }
  window.onresize = function() {
    if(controlTooLarge() && !isMobile())
      $("#control").width("177px");
    else
      $("#control").width("160px");
  }
  $("#legend").on("click","img",legendClick);
  // Add elements to grouping menu
  addAllGroupingEntries();
});

function controlTooLarge() {
  return(document.getElementById("control").scrollHeight > document.getElementById("control").offsetHeight);
}

function isMobile() {
  return(navigator.userAgent.match(/android/i) || navigator.userAgent.match(/iphone/i));
}

function hideMobileAddressBar() {
  $("#control").css("bottom","-50px");
  $("#map").css("bottom","-50px");
}

// }}}

function radioGroup(field, title) {    // {{{ Grouping
  return("<input type='radio' id='group_"+field+"' name='grupper' value='"+field+"'/><label for='group_"+field+"'>"+title+"</label><br>")
}

// Called whenever a new group is selected.
function groupClick(e) {
  var property=e.target.value;
  CurrentGroup=property;
  if (property=="nogroup") {
    Data.forEach(point => markerColor(point.marker,"blue"));
    closeLegend();
  }
  else {
    groupBy(property);
  }
}

function groupBy(property) {
  var group=Groups[property];
  // Initialize marker count for each bin/color to 0
  group.count.fill(0);
  for (var i=0;i<Data.length;i++) {
    groupPoint(Data[i], group, property);
  }
  setLegend(group);
}

function groupPoint(point, group, property) {
  var value;
  if (group.func_transform) value=group.func_transform(point);
  else value=point[property];

  for (var j=0;j<group.groups.length;j++) {
    if (value==group.groups[j].v || group.groups[j].v=="other") {
      point.vindex=j;
      markerColor(point.marker, group.groups[j].c);
      if (point.marker.getVisible())
        group.count[j]++;
      return;
    }
  }
  // Set strange color if points are not given any color
  markerColor(point.marker, "orange");
}

function addGrouping(DOMparent, menuTitle, legendTitle, property, groups, func_transform) {
  // Menu and legend titles can be different, but if no legendTitle then reuse menuTitle
  if (legendTitle==null) legendTitle=menuTitle;
  // Add DOM element
  DOMparent.append(radioGroup(property,menuTitle));
  var count=new Array(groups.length);
  // Add info to Groups variable, which is read 
  Groups[property]={title:legendTitle, groups:groups, func_transform:func_transform, count:count};
}

function addAllGroupingEntries() {
  var DOMparent=$("#grupper div:first");

  addGrouping(DOMparent, "Ingen grupper", null, "nogroup", []);
  // Init which group is shown as selected
  DOMparent.find("input")[0].checked=true
  //$("#group_nogroup").prop("checked", true)

  // Many groups needs a special "default" group for any new unplayed match without info yet
  var notplayed={v: "other", c: "purple", l: "Endnu ikke spillet"};

  addGrouping(DOMparent, "Kampresultat", "Resultat i udekamp","result",
    [{v: "w",        c: "green",  l: "Sejr"},
     {v: "d",        c: "yellow", l: "Uafgjort"},
     {v: "l",        c: "red",    l: "Nederlag"}, notplayed ]);

  addGrouping(DOMparent, "Rundens resultat", "Resultat af runde", "progres",
    [ {v: "w",        c: "green",  l: "Gået videre"},
      {v: "l",        c: "red",    l: "Slået ud"},
      {v: "g",        c: "blue",   l: "Gruppespil"}, notplayed ],
      function(p){
        if (p.win==1) return("w");if(p.win==0) return("l");if(p.part=="group") return("g"); return(null);
      }
  );

  addGrouping(DOMparent, "Turnering",null,"tournament",
     [ {v: "CL",       c: "green",  l: "Champions Leaugue"},
       {v: "EL",       c: "blue",   l: "Europa League"},
       {v: "other",    c: "red",    l: "Anden turnering"}]);

  addGrouping(DOMparent, "Runde",null,"part",
     [ {v: "quali",    c: "red",    l: "Kvalifikation"},
       {v: "main",     c: "blue",   l: "Hovedturnering"},
       {v: "group",    c: "yellow", l: "Gruppespil"},
       {v: "knockout", c: "green",  l: "Knockout"}]);

  const lims=[2003,2009,2012,2017,2100];
  const zeroPad = (num, places) => String(num).padStart(places, '0')
  let fullseason=function(s){return((s-1)+"/"+zeroPad(s%100,2));}
  let fseason=function(s1,s2){return(fullseason(s1)+" - "+fullseason(s2));}
  let seasongroup=function(lims) {
    return([{v:0, c:"red",    l:fseason(1993,lims[0])},
            {v:1, c:"yellow", l:fseason(lims[0]+1,lims[1])},
            {v:2, c:"green",  l:fseason(lims[1]+1,lims[2])},
            {v:3, c:"blue",   l:fseason(lims[2]+1,lims[3])},
            {v:4, c:"purple", l:fullseason(lims[3]+1)+" - I dag"}])};
  addGrouping(DOMparent,"Tidsperiode", null, "season", seasongroup(lims),
    function(p){for(var i=0;i<=lims[i];i++) if(p.season<=lims[i]) return(i);}
  );


  var goalgroup=function(n) {return([
    {v: 0,          c: "red",    l: "Ingen mål"},
    {v: 1,          c: "yellow", l: "1 mål "+n},
    {v: 2,          c: "green",  l: "2 mål "+n},
    {v: 3,          c: "blue",   l: "3 mål "+n},
    {v: 4,          c: "pink",   l: "4+ mål "+n},
    notplayed]);};
  addGrouping(DOMparent, "Mål scoret",null,"goalf",goalgroup("scoret"),function(p){return(p.goalf<4?p.goalf:4);});
  addGrouping(DOMparent, "Mål inkasseret",null,"goala",goalgroup("mod"),function(p){return(p.goala<4?p.goala:4)});

  addGrouping(DOMparent, "Tilskuertal", null, "tilskuere",
    [ {v: 1, c: "red",    l: "Under 5.000"},
      {v: 2, c: "yellow", l: "5.000-15.000"},
      {v: 3, c: "blue",   l: "15.000-30.000"},
      {v: 4, c: "green",  l: "Over 30.000"},
      notplayed],
    function(p){
      if(p.tilskuere==null) return(null); 
      if(p.tilskuere<5000)  return(1); 
      if(p.tilskuere<15000) return(2); 
      if(p.tilskuere<30000) return(3); 
      return(4);
    }
  );

  addGrouping(DOMparent, "Højdepunkter",null,"youtube",
    [ {v:"ja",  c:"green", l: "Med link til højdepunkter"},
      {v:"nej", c:"yellow", l: "Uden link til højdepunkter"},
      notplayed],
      function(p){if(p.youtube==null) return("other"); if(p.youtube==="") return("nej"); return("ja");}
    );

  addGrouping(DOMparent, "Seedning", null, "seeding",
    [ {v:1, c:"green", l:"FCK seedet"},
      {v:0, c:"red",   l:"FCK useedet"},
      {v:"other", c:"purple", l:"Ingen seedning"}]);

  addGrouping(DOMparent, "Indbyrdes kampe", "Antal indbyrdes opgøe", "indbyrdes",
    [ {v:1, c:"blue",   l: "1. indbyrdes opgør"},
      {v:2, c:"yellow", l: "2. indbyrdes opgør"},
      {v:"other", c:"green",  l: "3.+ indbyrdes opgør"}]);

  // The groupsBy function is triggered by all input elements to perform the grouping
  DOMparent.on("click","input",groupClick);
}

// }}}

function filterData(value) {           // {{{ Filtering
// Function called when any filter form input element is changed.
  filterParse(value);    // Read which filters are set from the DOM for this filter
  filterUpdate(value);   // Run through all markers, and show/hide based on filters
}

// Check all input elements for this filter-value, and update global
// filter list, which is used to select observations in filterUpdate
function filterParse(value) {
  var f = [];
  var categories = document.getElementsByName(value);
  if (value=="part" || value=="tourgroup") {
    for (var i = 0, category; category = categories[i]; i++) {
      if (category.checked) f.push(category.value);
    }
    filter[value]=f;
  }
  else if (value=="season") {
    var season_from = Math.min(categories[0].value,categories[1].value);
    var season_to   = Math.max(categories[0].value,categories[1].value);
    filter[value]=[season_from, season_to];
  }
  else if (value=="video") {
    if ($('input[name=video]:checked').val()=="alle")
      filter[value]=0;
    else
      filter[value]=1;
  }
}

function filterPoint(data) {
  for (var key of ["tourgroup","part"])
    if (!filter[key].includes(data[key])) return(false);
  if (!data.youtube && filter.video==1) return(false);
  if (data.season<filter.season[0] || data.season>filter.season[1]) return(false);
  // All filters OK. This datapoint should by visible
  return(true);
}

// Update map by selecting points with the currently active filters, found
// in the global variable filter
function filterUpdate(value) {
  for (var i=0;i<Data.length;i++) {
    Data[i].marker.setVisible(filterPoint(Data[i]));
  }
  if (CurrentGroup!="nogroup") groupBy(CurrentGroup);
}
// }}}

function oldstuff() {
  if (value=="") {
    for (var i=0;i<Data.length;i++) {
      markerColor(Data[i].marker,"blue");
    }
    closeLegend();
    return(0);
  }
  else console.log("Invalid group value: "+value);
}

function initMap() {                   // {{{ Initial setup of the map
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 50.95, lng:  14.7},
    zoom: 4,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
      position: google.maps.ControlPosition.TOP_RIGHT
    }
  });

  // Initialize global infoWIndow variable
  infoWindow = new google.maps.InfoWindow();
  // Start by populating the global filter arrat by parsing all filters
  filterParse("part");
  filterParse("tourgroup");
  filterParse("season");
  filterParse("video");

  // Call openIW when a marker is clicked and close it on any click on the map
  //google.maps.event.addListener(map, "click", openIW);
  google.maps.event.addListener(map, "click", function() {infoWindow.close();closeCredits();});

  $.ajax({url: "getdata.php", dataType: 'json', success: setupData });
}

function setupData(result) {
  Data=result;
  for (var i=0;i<result.length;i++) {
    d=Data[i];
    // Fix extra tournament variable
    if      (d.tournament=="CL") d.tourgroup="cl";
    else if (d.tournament=="EL") d.tourgroup="el";
    else d.tourgroup="andre";
    d.marker=addMarker(d);
  }
  setInitialMark();
}

// If a mark is specified as a get variable, initial_mark is set from php-file
function setInitialMark() {
  if (typeof initial_mark !== 'undefined') {
    groupBy(initial_mark);
    $("#group_"+initial_mark).prop("checked", true)
  }
}

function markerColor(marker,color) {
  if (color=="cyan")
    marker.setIcon("https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|00FFFF");
  else
    marker.setIcon("http://maps.google.com/mapfiles/ms/icons/"+color+"-dot.png");
}

function addMarker(data) { 
  var newMark = new google.maps.Marker({
    position: {lat: parseFloat(data.latitude), lng: parseFloat(data.longitude)},
    // animation: google.maps.Animation.DROP,
    map: map,
    icon: {url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"}
  })
  newMark.addListener('click', function() {
    makeInfo(data);
    infoWindow.open(map, newMark);
  });
  return(newMark);
} 
// }}}

// Create info window                  // {{{ Info window
function infoTourname(shortname) {
  if (shortname=="EL") return("Europa League");
  if (shortname=="CL") return("Champions League");
  if (shortname=="CW") return("Cup Winners Cup");
  if (shortname=="UC") return("UEFA cup");
}
function infoFinal(finaltype) {
  if (finaletype=="quarterfinal") return("Kvartfinale");
  if (finaletype=="semifinale")   return("Semifinale");
  if (finaletype=="final")        return("Finale");
}
function infoFinalValue(value) {
  if (value==1) return("1/16-finale");
  if (value==2) return("1/8-finale");
  if (value==3) return("Kvartfinale");
  if (value==4) return("Semifinale");
  if (value==5) return("Finale");
}
function infoRunde(part,round,groupround,tournament) {
  if (part=="group") return("Gruppespil - "+groupround+". runde")
  if (part=="main") return(round+". runde");
  if (part=="quali") {
    if (round==4) return("Playoff");
    else return(round+". kvalrunde");
  }
  if (part=="knockout") {
    var r=round+". runde - ";
    if (tournament=="UC") return(r+infoFinalValue(round-2));
    if (tournament=="EL") return(r+infoFinalValue(round-1));
    if (tournament=="CL") return(r+infoFinalValue(round));
  }
}
function infoStadium(stadium,wiki) {
  return("<span class='label'>Stadion:</span><span class='stadium'><a href='https://en.wikipedia.org/wiki/"+wiki+"' target='_blank'>"+stadium+"</a></span>");
}
function infoResult(goalf,goala) {
  if (goalf==null) return("Endnu ikke spillet")
  var res=goalf+"-"+goala;
  if (goalf>goala) return("Vundet "+res);
  if (goalf==goala) return("Uafgjort "+res);
  if (goalf<goala) return("Nederlag "+res);
}
function infoTilskuere(x) {
  if (x==0||x>0) x=x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
  else x="-"
  return("<span class='label'> Tilskuere: </span><span class='tilskuere'>"+x+"</span>");
}
function infoCoach(coach) {
  return("<span class='label'>Træner:</span>"+coach);
}
function infoSeeding(x) {
  var r="<span class='label'> Seedning: </span>";
  if (x=="1") return(r+"FC København seeded");
  if (x=="0") return(r+"FC København useedet");
  return(r+"Ingen");
}
function infoHjemmekamp(part,place1,homegf,homega,kampe) {
  var r;
  if (kampe<="1")    return(ilabel("I Parken")+"Ingen hjemmekamp");
  if (part=="group") r="Kampen";
  else if (place1 == "home") r="1. kamp";
  else if (place1 == "away")  r="Returkampen";
  return(ilabel("I Parken")+r+" i Parken endte "+homegf+"-"+homega);
}
function infoSamlet(kampe,agf,aga,hgf,hga,win,penalty) {
  if (win==null) return("");
  if (kampe==1) return("");
  var samlet=(parseInt(agf)+parseInt(hgf))+"-"+(parseInt(aga)+parseInt(hga));
  var res="<span class='label'>Samlet</span>"+(win=="1"?"Sejr":"Nederlag")+" på "+samlet+
      infoUdebanemaal(agf,aga,hgf,hga)+infoPenalty(penalty)+"<br>";
  return(res);
}
function infoUdebanemaal(agf,aga,hgf,hga) {
  agf=parseInt(agf);
  aga=parseInt(aga);
  hgf=parseInt(hgf);
  hga=parseInt(hga);
  if (hgf+agf != aga+hga) return("");
  if (hga!=agf) return(" (udebanemål)");
  return("");
}
function infoPenalty(penalty) {
  if (penalty==null) return("");
  var res=parseInt(parseInt(penalty)/100)+"-"+(parseInt(penalty)%100);
  return(" - straffespark "+res);
}
function infoIndbyrdes(x) {
  return(ilabel("Indbyrdes")+x+". indbyrdes møde i Europa");
}
function infoComment(x) {
  if (x==null || x=="") return("")
  return(ilabel("Kommentar:")+" <span class='infocomment'> "+x+"</span> <p>")
}
function infoNipser(date) {
  var base="https://nipserstat.dk/nipserstat/SimpleGameDetails?gamedate=";
  var date=date.substring(8,10)+"/"+date.substring(5,7)+"/"+date.substring(0,4);
  return("Se hos <a href='"+base+date+"' target='_blank'>Nipserstat</a>");
}
function infoHighlights(token) {
  if (token!="") {
    return("Se <a href='https://www.youtube.com/watch?v="+token+"' target='_blank'>Highlights</a>");
  }
  return("Ingen video");
}
function ilabel(n) { return("<span class='label'> "+n+":</span>") }

function makeInfo(d) {
  var content="<div class='infowindow'>"+
              "Udekamp nr: "+d.id+" i Europa<br>"+
              infoTourname(d.tournament)+" - "+infoRunde(d.part,d.round,d.groupround,d.tournament)+"<p>"+
              "<span class='label'> Sæson: </span><b>"+(d.season-1)+"/"+d.season+"</b><br>"+
              "<span class='label'> Dato:  </span><b>"+d.date+"</b><br>"+
              infoCoach(d.coach)+"<br>"+
              infoSeeding(d.seeding)+"<p>"+
              infoStadium(d.stadium,d.wiki)+"<p>"+
              "<span class='label'>Modstander:</span>"+
              "<span class='opponent'>"+d.opponent+"</span> <span class='country'>("+d.oppc+")</span><p>"+
              infoTilskuere(d.tilskuere)+"<br>"+
              "<span class='label'> Resultat: </span><span class='result'>"+infoResult(d.goalf,d.goala)+"</span><br>"+
              infoHjemmekamp(d.part,d.place1,d.homegf,d.homega,d.kampe)+"<br>"+
              infoSamlet(d.kampe,d.goalf,d.goala,d.homegf,d.homega,d.win,d.penalty)+
              infoIndbyrdes(d.indbyrdes)+"<p>"+
              infoComment(d.comment)+
              infoNipser(d.date)+" &nbsp; - &nbsp; "+
              infoHighlights(d.youtube);
              "";
  infoWindow.setContent(content);
} // }}}

function closeLegend() {
  $("#legend").hide();
}

function openLegend() {
  $("#legend").show();
}

function closeCredits() {
  $('#feedback').hide();
}

function setLegend(group) {
  //console.time("DOMlookup");
  //console.timeEnd("DOMlookup");
  $("#legendtitle").html("<b>"+group.title+"</b>");
  $("#legend .legendentry").remove();
  for (var i=0,entry; entry=group.groups[i]; i++) {
    // Only add legend group if there are any visible markers in this group
    if (group.count[i]) {
      $("#legend").append("<div class='legendentry'><img src='http://maps.google.com/mapfiles/ms/micons/"+entry.c+"-dot.png' data-lindex='"+i+"'> "+entry.l+" ("+group.count[i]+")</div>");
      //Make call-back in a "double" closure, so value of i is preserved - but does not work, because callback is set on ALL entries everytime
      //$("#legend").on("click","img",function(aa){return(function(){legendClick(aa);})}(i));
    }
  }
  openLegend();
}

function legendClick(e) {
  var lindex=e.target.dataset.lindex;
  Data.forEach(function(p){if(p.vindex==lindex) p.marker.setAnimation(google.maps.Animation.BOUNCE);});
  setTimeout(function(){stopBounce(lindex);},1000);
}

function stopBounce(lindex) {
  Data.forEach(function(p){if(p.vindex==lindex) p.marker.setAnimation(google.maps.Animation.NONE);});
}
  

// Print all keys of an object to the console for debugging
function keylist(o) {
  console.log(Object.keys(o));
}
  
