drop view if exists fckeuropa_source;
create view fckeuropa_source as select id,season,if(place1 = 'away',date1,date2) AS date,tournament,part,final,round,roundtotal,groupround,oid,opponent,oppc,place1,seeding,if(place1 = 'away',gf1,gf2) AS goalf,if(place1 = 'away',ga1,ga2) AS goala,kampe,if(place1='away',gf2,gf1) as homegf,if(place1='away',ga2,ga1) as homega,win,penalty from europacup.matchtarget where target = 'FC København' and (kampe = 2 or (kampe <= 1 and place1 = 'away'));

drop view if exists fckeuropa_errors;
create view fckeuropa_errors as select a.id,"extra" as source,a.date,a.opponent,b.date as datematch from fckeuropa_extra as a left join fckeuropa_source as b on a.date=b.date having isnull(datematch) union select a.id,"source" as source,a.date,a.opponent,b.date as datematch from fckeuropa_source as a left join fckeuropa_extra as b on a.date=b.date having isnull(datematch);

drop view if exists fckeuropa_view;
create view fckeuropa_view as 
  select e.id as id, 
         s.id as matchid,
         season,
         s.date,
         tournament,
         part,
         final,
         round,
         roundtotal,
         groupround,
         oid,
         s.opponent,
         oppc,
         place1,
         seeding,
         goalf,
         goala,
         if (goalf>goala,"w",if(goalf=goala,"d",if(goalf<goala,"l",NULL))) as result,
         kampe,
         homegf,
         homega,
         win,
         if (win=1,"w",if(win=0,"l",if(part="group","g",null))) as progres,
         penalty,
         stadium,
         wiki,
         rank() over (partition by s.opponent order by s.date) as indbyrdes,
         coach,
         tilskuere,
         latitude,
         longitude,
         if (youtube,1,0) as hasvideo,
         youtube,
         comment,
         e.opponent as nipsopponent 
  from fckeuropa_source as s join fckeuropa_extra as e on s.date=e.date order by s.date;

drop table if exists fckeuropa;
create table fckeuropa as select * from fckeuropa_view;

/* Create another source table with matches for ALL danish teams
   I might use this to make a version for other teams as well   */
drop view if exists dkeuropa_source;
create view dkeuropa_source as select id,season,if(place1 = 'away',date1,date2) AS date,tid,target,tournament,part,final,round,roundtotal,groupround,oid,opponent,oppc,place1,seeding,if(place1 = 'away',gf1,gf2) AS goalf,if(place1 = 'away',ga1,ga2) AS goala,kampe,if(place1='away',gf2,gf1) as homegf,if(place1='away',ga2,ga1) as homega,win,penalty from europacup.matchtarget where targetc = 'Den' and (kampe = 2 or (kampe <= 1 and place1 = 'away'));
