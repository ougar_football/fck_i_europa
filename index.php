<!DOCTYPE html>
<html>
  <head>
    <title> FCK i Europa </title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@kghougaard" />
    <meta name="twitter:title" content="FC Københavns kampe i Europa" />
    <meta name="twitter:description" content="Interaktivt kort over alle udekampe FC København har spillet i de europæiske turneringer" />
    <meta name="twitter:image" content="http://fodbold.ougar.dk/fckobenhavn/europakort/img/fckeuropa.png" />
    <link rel="stylesheet" type="text/css" href="fckeuropa.css">
    <script type="text/javascript" src="http://ougar.dk/javascript/jquery-1.8.min.js"></script>
    <script type="text/javascript" src="fckeuropa.js"></script>
<?php if(array_key_exists("mark",$_GET)) print("    <script type='text/javascript'> var initial_mark='".$_GET['mark']."'; </script>\n"); ?>
  </head>
  <body>
    <div id="header">
      <h1> FC Københavns udekampe i Europa </h1>
    </div>
    <!-- Left menu div - includes all filters defined below and group-buttons added in javascript file -->
    <div id="control">
      <div id="select">
        <h3>&nbsp;Vælg kampe</h3>
        <div class='controlgroup'>
          <h4>Turnering</h4>
          <input type='checkbox' id='t1' checked='checked' name='tourgroup' value='cl'    onClick='filterData("tourgroup")'> <label for='t1'> Champions League </label><br>
          <input type='checkbox' id='t2' checked='checked' name='tourgroup' value='el'    onClick='filterData("tourgroup")'> <label for='t2'> Europa League    </label><br>
          <input type='checkbox' id='t3' checked='checked' name='tourgroup' value='andre' onClick='filterData("tourgroup")'> <label for='t3'> Andre            </label><br>
        </div>
        <div class='controlgroup'>
          <h4>Runder</h4>
          <input type='checkbox' id='r1' checked='checked' name='part' value='quali'    onClick='filterData("part")'> <label for='r1'> Kvalifikation  </label> <br>
          <input type='checkbox' id='r2' checked='checked' name='part' value='main'     onClick='filterData("part")'> <label for='r2'> Hovedturnering </label> <br>
          <input type='checkbox' id='r3' checked='checked' name='part' value='group'    onClick='filterData("part")'> <label for='r3'> Gruppespil     </label> <br>
          <input type='checkbox' id='r4' checked='checked' name='part' value='knockout' onClick='filterData("part")'> <label for='r4'> Knockout       </label> <br>
        </div>
        <div class='controlgroup'>
          <h4>Sæsoner</h4>
          <span class='tlabel'>Fra</span><select name='season' onChange='filterData("season")'> 
<?php for ($i=1993;$i<2022;$i++) print("            <option value='$i'>".($i-1)."/$i\n"); ?>
          </select><br>
          <span class='tlabel'>Til</span><select name='season' onChange='filterData("season")'> 
<?php for ($i=1993;$i<2022;$i++) print("            <option value='$i'".($i==2021?" selected":"").">".($i-1)."/$i\n"); ?>
          </select><br>
        </div>
        <div class='controlgroup'>
          <h4>Highlights</h4>
            <input type="radio" id='video_all'   name='video' value='alle'  onChange='filterData("video")' checked /> <label for='video_all'  >Alle kampe </label><br>
            <input type="radio" id='video_video' name='video' value='video' onChange='filterData("video")' />         <label for='video_video'>Med højdepunkter </label>
        </div>
      </div>
      <div id='grupper'>
        <h3>&nbsp;Gruppér kampe </h3>
        <div class='controlgroup'>
          <!-- group-by sections added dynamically with javascript -->
        </div>
      </div>
      <div id="contact"> 
        <div class='controlgroup'>
          <span>&nbsp;&nbsp;Kontakt &amp; Kreditering</span>
        </div>
      </div>
    </div>
    <!-- Tiny many icon has its own div. An ugly solution. I should change that -->
    <div id="toggle"><img id='menuicon' src="img/menu_light.png"></div>
    <!-- Map area. This div is 'handed over' to google maps API -->
    <div id="map"></div>
    <!-- Legend div, which is shown every time a user selects to color by a specific property -->
    <div id="legend">
      <div id='close'><span id='legendtitle'>Signaturforklaring</span><img src='img/close.png' onClick='closeLegend()'></div>
    </div>
    <!-- Feedback div which is shown when user clicks "contact & credit" menu item -->
    <div id="feedback">
      <div id='close'><span> <b> FCK i Europa </b></span><img src='img/close.png' onClick='closeCredits()'></div>
      Lavet af <a href='http://fodbold.ougar.dk' target='_blank'>Kristian Hougaard (ougar)</a><br>
      Twitter: <a href='http://twitter.com/kghougaard' target='_blank'>@kghougaard</a><br>
      www: <a href='https://superstats.dk'>Superstats.dk</a><br>
      Koden ligger på <a href='https://gitlab.com/ougar_football/fck_i_europa'>Gitlab</a><p>
      Data bl.a. hugget fra <b><a href='http://www.nipserstat.dk' target='_blank'>Nipserstat</a></b>&nbsp;<img id='worship' src='img/icon_worship.gif' height='20px'><p>
      Skriv på twitter eller mail på<br>
      me-at-ougar.dk eller find mig på<br>
      <a href='http://www.sidelinien.dk' target='_blank'>sidelinien.dk</a> hvis du har<br>
      kommentarer, idéer eller måske vil<br>
      bidrage med et videolink <img id='happy' src='img/happy.png' height='16px'><p>
      Der er også mulighed for at lave<br>
      det samme for andre klubber, men<br>
      så skal man selv lave en smule af <br>
      fodarbejdet 😊. Sig til hvis det<br>
      er noget du synes lyder sjovt.
    </div>
    <!-- Load the google maps API. When done call my initMap function in my javascript file -->
    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAdT-UUA9eVi1KWl57UF1o_4IGhQfWM3cA&callback=initMap">
    </script>
  </body>
</html>
